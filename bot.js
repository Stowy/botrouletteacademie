// Run dotenv
require('dotenv').config();

// Import libraries
// eslint-disable-next-line no-unused-vars
const { Client, Message, User, GuildMember } = require('discord.js');
const { Sequelize, Op } = require('sequelize');
const fs = require('fs');

// Create a client
const client = new Client();

// Consts
const MIN_PLAYERS = 2;
const WAIT_TIME = 2000;
const NBR_MAX_PROTECTED_PLAYERS = 5;
const MAX_TIER = 5;
const MIN_TIER = 1;
const IS_IRL_TRUE_TEXT = 'TRUE'; // Text to put in isIrl for it to be true

// Vars
const welcomeMessages = [];
const shootMessages = [];
let areRegistrationsRuning = false;
let areRegistrationsReopened = false;
let canKill = false;
let recentlyRegisteredPlayers = [];
let registeredPlayers = [];
let unassociatedPlayers = [];
let survivingPlayers = [];
let gunPosition = 0;
/**
 * @type {Message}
 */
let lastDisplayChallengesMessage = null;

// Initialise Sequelize
const sequelize = new Sequelize({
    host: 'localhost',
    dialect: 'sqlite',
    logging: false,
    storage: 'challenges.db',
});

// Define model of the DB
const Challenges = sequelize.define('challenges', {
    challengeId: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        unique: true,
        allowNull: false,
        primaryKey: true,
    },
    challengeName: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    challengeTier: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    challengeIsIrl: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0,
    },
    challengeChallengerId: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    challengeNbAssociatedChallengers: {
        type: Sequelize.INTEGER,
        allowNull: true,
    },
    challengeAssociatedChallengersId: {
        type: Sequelize.ARRAY(Sequelize.TEXT),
        allowNull: true,
    },
});

/**
 * Represents a challenger.
 */
class Challenger {
    /**
     * Construct a challenger
     * @param {User} player Player (User)
     * @param {boolean} irl IRL Status
     */
    constructor(player, irl) {
        this.player = player;
        this.irl = irl;
    }
}

// Event listener when a user connects to the server
client.once('ready', () => {
    // Create the DB.
    Challenges.sync();
    console.log(`Logged in as ${client.user.tag}!`);
});

// Event listener when a user sends a message in the chat
client.on('message', (msg) => {
    // Check if the message has the prefix
    if (msg.content.startsWith(process.env.PREFIX)) {
        const input = msg.content
            .substring(process.env.PREFIX.length)
            .split(' ');
        const command = input.shift();
        // TODO : Clean le join qui est absolument dégolass
        const args = input.join(' ');
        console.log(command);

        sendCommand(msg, command, args);
    }
});

/**
 * Function to call to send a command.
 * @param {Message} msg Message object of the command.
 * @param {string} command Command to execute.
 * @param {string[]} args Arguments.
 */
async function sendCommand(msg, command, args = 0) {
    if (command === 'check') {
        // Starts a registration.
        // Check if the command author has administrator permissions and if registrations are not allready runing.
        if (!msg.member.hasPermission('ADMINISTRATOR')) {
            msg.reply("t'as pas les permissions administrateur.");
        } else if (areRegistrationsRuning) {
            msg.reply('un enregistrement est déjà en cours.');
        } else {
            commandCheck(msg, !args ? args[0] : 0);
            msg.delete();
        }
    } else if (command === 'in') {
        // Registers a player
        args = args.split(' ');
        if (areRegistrationsRuning) {
            if (
                args.length != 1 ||
                args[0] === '' ||
                args[0] === 0 ||
                args[0] === 1
            ) {
                msg.reply(
                    'précise si tu souhaites faire des défis IRL ou non !\n' +
                        `**Utilisation**: \`${process.env.PREFIX}in [1 si IRL OK, 0 sinon]\`  (Exemple : \`${process.env.PREFIX}in 1\` )`,
                );
                msg.delete();
            } else {
                commandIn(msg, args[0]);
            }
        } else {
            msg.reply('les inscriptions ne sont pas ouvertes.');
        }
    } else if (command === `kill`) {
        // Picks a player to kill
        if (
            canKill &&
            !areRegistrationsRuning &&
            msg.member.hasPermission('ADMINISTRATOR')
        ) {
            commandKill(msg);
        }
    } else if (command === 'stop') {
        if (msg.member.hasPermission('ADMINISTRATOR')) {
            // Stops the check-in
            endRegistrations(msg);
        }
    } else if (command === 'add_challenge') {
        // Adds a challenge
        args = args.split(';');
        if (!msg.member.hasPermission('ADMINISTRATOR')) {
        } else if (
            args.length < 3 ||
            args.length > 4 ||
            args[0] === '' ||
            args[1] === '' ||
            args[2] === '' ||
            args[3] === ''
        ) {
            msg.reply(
                'pas les bons arguments sur la commande.\n' +
                    `Utilisation: ${process.env.PREFIX}add_challenge [tier];[name];[irl];[nbAssociatedChallengers]. ` +
                    `(mettre ${IS_IRL_TRUE_TEXT} dans [irl] pour dire que c'est un défis irl)`,
            );
        } else if (args[0] < MIN_TIER || args[0] > MAX_TIER) {
            msg.reply(
                `le tier doit se trouver entre ${MIN_TIER} et ${MAX_TIER}`,
            );
        } else {
            commandAddChallenge(
                msg,
                Number(args[0]),
                args[1],
                args[2] == IS_IRL_TRUE_TEXT,
                args[3],
            );
        }
    } else if (command === 'display_challenge') {
        if (!msg.member.hasPermission('ADMINISTRATOR')) {
        } else if (args === '' || Array.isArray(args)) {
            msg.reply(
                'pas les bons arguments sur la commande.\n' +
                    `Utilisation: ${process.env.PREFIX}display_challenge [id].`,
            );
        } else {
            commandDisplayChallenge(msg, args);
        }
    } else if (command === 'display_challengers') {
        if (msg.member.hasPermission('ADMINISTRATOR')) {
            // Displays all challengers
            commandDisplayChallengers(msg);
        }
    } else if (command === 'display_survivors') {
        if (msg.member.hasPermission('ADMINISTRATOR')) {
            // Displays all survivors
            commandDisplaySurvivors(msg);
        }
    } else if (command === 'reopen') {
        if (!msg.member.hasPermission('ADMINISTRATOR')) {
            msg.reply("t'as pas les permissions administrateur.");
        } else if (areRegistrationsRuning) {
            msg.reply('un enregistrement est déjà en cours.');
        } else {
            commandReopen(msg, !args ? args[0] : 0);
        }
    } else if (command === 'remove_all_challenges') {
        if (!msg.member.hasPermission('ADMINISTRATOR')) {
            msg.reply("t'as pas les permissions administrateur.");
        } else {
            // Delete all the challenges from the DB
            await Challenges.destroy({ truncate: true });
            msg.channel.send('Tous les challenges ont bien été supprimé !');
        }
    } else if (command === 'display_all_challenges') {
        if (!msg.member.hasPermission('ADMINISTRATOR')) {
            msg.reply("t'as pas les permissions administrateur.");
        } else {
            commandDisplayAllChallenges(msg);
        }
    } else if (command === 'add_challenges') {
        // Check if the user has administrator persmissions
        if (msg.member.hasPermission('ADMINISTRATOR')) {
            // Get every line of challenge
            args = args.split('\n');

            // Remove the command for the list
            args.shift();

            // Get every challenges
            args.forEach((arg) => {
                arg = arg.split(';');
                if (
                    arg.length < 3 ||
                    arg.length > 4 ||
                    arg[0] === '' ||
                    arg[1] === '' ||
                    arg[2] === ''
                ) {
                    msg.reply('mauvais arguments.');
                } else {
                    commandAddChallenge(
                        msg,
                        Number(arg[0]),
                        arg[1],
                        arg[2] == IS_IRL_TRUE_TEXT,
                        arg[3],
                    );
                }
            });
        }
    } else if (command === 'force_trade_challenge') {
        // Check if the user has administrator persmissions
        if (msg.member.hasPermission('ADMINISTRATOR')) {
            // Get every line of challenge
            args = args.split(' ');

            // Remove the command for the list
            args.shift();

            if (args.length != 2 || args[0] === '' || args[1] === '') {
                msg.reply('mauvais arguments.');
            } else {
                commandForceTradeChallenge(msg, args[0], args[1]);
            }
        }
        /* } else if (command === 'trade_challenge') {
        // Get every line of challenge
        args = args.split(' ');

        if (args.length != 1 || args[0] === '') {
            msg.reply('mauvais arguments.');
        } else {
            commandTradeChallenge(msg, args[0]);
        }*/
    } else if (command === 'remove_challenge') {
        if (
            (msg.member.hasPermission('ADMINISTRATOR') && args != '') ||
            !Array.isArray(args)
        ) {
            const rowCount = await Challenges.destroy({
                where: { challengeId: args },
            });
            if (!rowCount) {
                msg.reply("ce challenge n'existe pas.");
            } else {
                msg.reply('challenge supprimé.');
            }
        }
    } else if (command === 'pick') {
        if (
            msg.member.hasPermission('ADMINISTRATOR') &&
            registeredPlayers.length > 0
        ) {
            const challenger =
                registeredPlayers[
                    Math.floor(Math.random() * registeredPlayers.length)
                ];
            msg.channel.send(`${challenger.player.toString()} à été choisi.`);
        }
    } else if (command === 'display_challenges') {
        if (msg.member.hasPermission('ADMINISTRATOR')) {
            commandDisplayChallenges(msg);
        }
    } else if (command === 'survivors') {
        if (msg.member.hasPermission('ADMINISTRATOR')) {
            msg.reply(survivingPlayers.length());
        }
    }
}

/**
 * Starts the registration of the players.
 * @param {Message} msg Message object of the command.
 * @param {number}  duration Duration of the registration in seconds. If equal to 0, you will have to stop the check-in manually.
 */
function commandCheck(msg, duration = 0) {
    registeredPlayers = [];
    unassociatedPlayers = [];
    survivingPlayers = [];
    areRegistrationsRuning = true;
    areRegistrationsReopened = false;
    canKill = false;
    gunPosition = 0;
    clearChallengersFromChallenges();

    const welcomeMessagesStr = fs.readFileSync('welcome_msgs.txt', 'utf8');
    const wlclines = welcomeMessagesStr.split('\n');
    for (line = 0; line < wlclines.length; line++) {
        welcomeMessages.push(wlclines[line]);
    }

    const shootMessagesStr = fs.readFileSync('shoot_msgs.txt', 'utf8');
    const shlines = shootMessagesStr.split('\n');
    for (line = 0; line < shlines.length; line++) {
        shootMessages.push(shlines[line]);
    }

    let confirmationMessage = "";

    if (duration > 0) {
        confirmationMessage +=
            `\nVous avez ${duration} seconde` + duration > 1 ? 's' : '' + '.';
    } else {
        // confirmationMessage += `\nUtilisez la commande \`${process.env.PREFIX}stop\` pour arrêter l'enregistrement.`;
    }
    confirmationMessage +=
        `\n-------------\n` +
        `------------- **INSCRIPTIONS À LA SHEN'ZOULETTE** \n` +
        `-------------\n` +
        `Utilisez la commande \`${process.env.PREFIX}in [0 | 1]\` pour vous inscrire.`;

    msg.channel.send(confirmationMessage);

    // Ends the registration if there is a duration
    if (duration > 0) {
        setTimeout(function () {
            endRegistrations(msg);
        }, duration * 1000);
    }
}

/**
 * Registers the user.
 * @param {Message} msg Message object of the command.
 * @param {number} irl If the user want an irl challenge or not.
 */
async function commandIn(msg, irl) {
    welcomeMessage = "> " + welcomeMessages[
        Math.floor(Math.random() * welcomeMessages.length)
    ]
        .toString()
        .replace('$', msg.author);
    const irlBoolean = irl == 1;
    const challenger = new Challenger(msg.author, irlBoolean);
    let challengerExists = false;
    registeredPlayers.forEach((existingChallenger) => {
        if (existingChallenger.player.id == challenger.player.id) {
            challengerExists = true;
        }
    });
    if (challengerExists) {
        msg.reply('pas besoin de spam ! Tu fais déjà parti des participants !');
    } else {
        if (!areRegistrationsReopened) {
            // Register player
            registeredPlayers.push(challenger);
            msg.channel.send(welcomeMessage);
        } else {
            // Register new player
            registeredPlayers.push(challenger);
            recentlyRegisteredPlayers.push(msg.author);
            msg.channel.send(welcomeMessage);
        }
    }
    msg.delete();
}

/**
 * Picks a player and removes him from the list
 * @param {Message} msg Message object of the command.
 */
async function commandKill(msg) {
    let isPlayerAlive = true;
    let nbrTry = 0;

    msg.channel.send(
        `\n-------------\n` +
            `------------- **LANCEMENT DE LA PARTIE** \n` +
            `-------------\n`,
    );
    // The number of players to protect at the begining of the round to make more suspense
    const nbrProtectedPlayers = Math.floor(
        Math.random() * NBR_MAX_PROTECTED_PLAYERS,
    );

    while (isPlayerAlive) {
        // Choose a random player
        const currentChallenger = survivingPlayers[gunPosition];

        // Check if the player is protected
        const isProtected = nbrTry <= nbrProtectedPlayers;

        // Compute the chances of dying
        let maxRandom = 4;

        // Max out at 1 so that we allways have 1/2 chances of dying
        if (maxRandom < 1) {
            maxRandom = 1;
        }

        // Check if the player is alive
        const isAlive = Math.floor(Math.random() * maxRandom) > 0;
        isPlayerAlive = isProtected || isAlive;

        shootMessage = "> " + shootMessages[
            Math.floor(Math.random() * shootMessages.length)
        ]
            .toString()
            .replace('$', currentChallenger.player.toString() + " 🔫");

        // Print the selected player
        msg.channel.send(shootMessage);

        // Wait before printing the result
        await sleep(WAIT_TIME);

        // Check if the player is still alive
        if (isPlayerAlive) {
            // Print if he survived or not
            msg.channel.send('Il survit. 🙏');

            // Wait before testing an other player
            await sleep(WAIT_TIME);

            // Increment the number of trys
            nbrTry++;

            // Change the position of the gun
            gunPosition++;
            // Makes the gun loop back to the begining
            if (gunPosition >= survivingPlayers.length) {
                gunPosition = 0;
            }
        } else {
            msg.channel.send('<:bang:706594112480083998>');
            msg.channel.send('IL MEURT. 💀');

            // give the player a challenge
            await giveChallenge(msg, currentChallenger);

            // Remove the player from the survivingPlayers array.
            let deadPlayerIndex = -1;
            for (let i = 0; i < survivingPlayers.length; i++) {
                if (
                    survivingPlayers[i].player.id == currentChallenger.player.id
                ) {
                    deadPlayerIndex = i;
                }
            }
            if (deadPlayerIndex > -1) {
                survivingPlayers.splice(deadPlayerIndex, 1);
                if (gunPosition == survivingPlayers.length) {
                    gunPosition = 0;
                }
            } else {
                msg.channel.send(
                    "La Shen'Zoulette a rencontré un petit problème xd^^",
                );
            }

            // Display the winner
            if (survivingPlayers.length == 1) {
                canKill = false;
                registeredPlayers = [];
                unassociatedPlayers = [];
                gunPosition = 0;
                msg.channel.send(`GG ${survivingPlayers[0].player.toString()}`);
            }
        }
    }

    msg.delete();
}

/**
 * Reopens the registration of the players.
 * @param {Message} msg Message object of the command.
 * @param {number}  duration Duration of the registration in seconds. If equal to 0, you will have to stop the check-in manually.
 */
function commandReopen(msg, duration = 0) {
    recentlyRegisteredPlayers = [];
    areRegistrationsRuning = true;
    areRegistrationsReopened = true;
    canKill = false;
    gunPosition = 0;

    let confirmationMessage = `L'enregistrement est ré-ouvert ! \nUtilisez la commande \`${process.env.PREFIX}in\` pour vous inscrire et rejoindre les participants déjà existants.`;
    if (duration > 0) {
        confirmationMessage +=
            `\nVous avez ${duration} seconde` + duration > 1 ? 's' : '' + '.';
    } else {
        confirmationMessage += `\nUtilisez la commande \`${process.env.PREFIX}stop\` pour arrêter l'enregistrement.`;
    }
    confirmationMessage +=
        `\n-------------\n` +
        `------------- **ENREGISTREMENT DE JOUEURS** \n` +
        `-------------\n`;

    msg.channel.send(confirmationMessage);

    // Ends the registration if there is a duration
    if (duration > 0) {
        setTimeout(function () {
            endRegistrations(msg);
        }, duration * 1000);
    }
}

/**
 * Add a challenge to the DB.
 * @param {Message} msg Message of the command.
 * @param {number} tier The tier of the challenge.
 * @param {string} name What the challenge is.
 * @param {boolean} isIrl If the challenge includes IRL elements or not.
 * @param {number} nbAssociatedChallengers Number of Associated Challengers (can be null)
 * @return {Promise<Message[]>}
 */
async function commandAddChallenge(
    msg,
    tier,
    name,
    isIrl,
    nbAssociatedChallengers,
) {
    try {
        const challenge = await Challenges.create({
            challengeName: name,
            challengeTier: tier,
            challengeIsIrl: isIrl,
            challengeNbAssociatedChallengers: nbAssociatedChallengers,
        });

        msg.reply(
            `le challenge: ${challenge.challengeName} à bien été ajouté. Il a l'id: ${challenge.challengeId}.`,
        );
    } catch (e) {
        if (e.name === 'SequelizeUniqueConstraintError') {
            return msg.reply('Ce challenge existe déjà.');
        }
        return msg.reply(
            "Quelque chose s'est mal passé avec l'ajout du challenge.",
        );
    }
}

/**
 * Command to display one challenge with his id.
 * @param {Message} msg Message object of the command.
 * @param {string} id Id of the challenge to display.
 */
async function commandDisplayChallenge(msg, id) {
    // Find the challenge with the good id
    const challenge = await Challenges.findOne({ where: { challengeId: id } });

    // Check if we found a challenge
    if (challenge) {
        // Display the challenge
        msg.channel.send(
            `Challenge: ${challenge.get('challengeName')}\n` +
                `Tier: ${challenge.get('challengeTier')}\n` +
                `Irl ?: ${challenge.get('challengeIsIrl') ? 'oui' : 'non'}`,
            `NbAssociatedPlayers : ${
                challenge.get('challengeNbAssociatedChallengers')
                    ? challengeNbAssociatedChallengers
                    : 'Pas de challengers associés'
            }`,
        );
    } else {
        // No challenge found with that id
        msg.reply(`pas de challenge avec l'id: ${id}.`);
    }
}

/**
 * Display all the challenges present in the DB.
 * @param {Message} msg Message object of the command.
 */
async function commandDisplayAllChallenges(msg) {
    // Get all the challenges
    const challenges = await Challenges.findAll();

    let message = '';

    challenges.forEach((challenge) => {
        message +=
            `${challenge.get('challengeName')}, ` +
            `tier: ${challenge.get('challengeTier')}, ` +
            `${challenge.get('challengeIsIrl') ? 'irl' : 'pas irl'},` +
            `id: ${challenge.get('challengeId')}\n`;
        if (
            challenge.get('challengeChallengerId') != null &&
            challenge.get('challengeAssociatedChallengersId') == null
        ) {
            message += `Joueur: <@${challenge.get(
                'challengeChallengerId',
            )}>\n\n`;
        } else if (
            challenge.get('challengeChallengerId') != null &&
            challenge.get('challengeNbAssociatedChallengers') != null
        ) {
            message +=
                `Joueur: <@${challenge.get('challengeChallengerId')}>\n` +
                `Joueurs associés : <@${challenge.get(
                    'challengeChallengerId',
                )}>\n\n`;
        }
    });

    if (challenges) {
        msg.channel.send(message);
    } else {
        msg.channel.send('Pas de challenges existants.');
    }
}

/**
 * Force trade challenge between 2 challengers
 * @param {Message} msg Message object of the command.
 * @param {User} challenger1 Challenger 1
 * @param {User} challenger2 Challenger 2
 */
async function commandForceTradeChallenge(msg, challenger1, challenger2) {
    const challenge1 = await Challenges.findOne({
        where: { challengeChallengerId: challenger1.id },
    });
    const challenge2 = await Challenges.findOne({
        where: { challengeChallengerId: challenger2.id },
    });

    const idChallenge1 = challenge1.challengeId;
    const idChallenge2 = challenge2.challengeId;

    const updatedChallenge1 = await Challenges.update(
        { challengeChallengerId: challenger2.id },
        { where: { challengeId: idChallenge1 } },
    );

    const updatedChallenge2 = await Challenges.update(
        { challengeChallengerId: challenger1.id },
        { where: { challengeId: idChallenge2 } },
    );

    if (updatedChallenge1 && updatedChallenge2) {
        console.log('Challenges tradés');
    }
}

/**
 * Ask trade challenge to another challenger
 * @param {Message} msg Message object of the command.
 * @param {String} challengerMention Challenger with whom he wants to switch
 */
async function commandTradeChallenge(msg, challengerMention) {
    let challengerId = challengerMention.slice(2, -1);

    if (challengerId.startsWith('!')) {
        challengerId = challengerId.slice(1);
    }

    console.log(challengerId);

    const challenger = client.users.fetch(challengerId);

    const sentMessage = await msg.channel.send(
        `Hey ${(
            await challenger
        ).toString()}, un certain ${msg.author.toString()} souhaite échanger son challenge avec le tien.\nSi tu es d'accord, réagis avec un 👍 à ce message !`,
    );
    sentMessage.react('👍');
}

/**
 * Display the challenges that where given to people.
 * @param {Message} msg Message object of the command.
 */
async function commandDisplayChallenges(msg) {
    const challengeMessage = await getChallengesMessage();
    lastDisplayChallengesMessage = await msg.channel.send(challengeMessage);
    msg.delete();
}

/**
 * Display all challengers
 * @param {Message} msg Message object of the command.
 */
async function commandDisplayChallengers(msg) {
    let listOfPlayers = ``;
    if (registeredPlayers) {
        listOfPlayers += `**• Liste des participants retenus :**\n`;
        areRegistrationsReopened = false;
        registeredPlayers.forEach(function (challenger) {
            listOfPlayers += `> - ` + challenger.player.toString() + `\n`;
        });
        if (listOfPlayers) {
            msg.channel.send(listOfPlayers);
        }
    }
}

/**
 * Display all survivors
 * @param {Message} msg Message object of the command.
 */
async function commandDisplaySurvivors(msg) {
    let listOfSurvivors = ``;
    if (survivingPlayers) {
        listOfSurvivors +=
            `Il reste ${survivingPlayers.length} survivants !\n` +
            `**• Liste des survivants :**\n`;
        survivingPlayers.forEach(function (challenger) {
            listOfSurvivors += `> - ` + challenger.player.toString() + `\n`;
        });
        if (listOfSurvivors) {
            msg.channel.send(listOfSurvivors);
        }
    }
}

/**
 * Updates the message that lists the given challenges.
 */
async function updateDisplayChallenge() {
    if (lastDisplayChallengesMessage != null) {
        const challengeMessage = await getChallengesMessage();

        lastDisplayChallengesMessage.edit(challengeMessage);
    }
}

/**
 * Get the message that displays the given challenges.
 * @return {Promise<string>}
 */
async function getChallengesMessage() {
    // Get the challenges
    const challenges = await Challenges.findAll({
        where: { challengeChallengerId: { [Op.ne]: null } },
    });

    let message = 'Liste des challenges attribués :\n';

    challenges.forEach((challenge) => {
        message += `> Challenge "${challenge.get(
            'challengeName',
        )}" - à réaliser par <@${challenge.get('challengeChallengerId')}>.\n`;
    });

    return message;
}

/**
 * Give a challenge to a player.
 * @param {Message} msg Message object of the command.
 * @param {User} challenger Challenger to give a challenge to.
 */
async function giveChallenge(msg, challenger) {
    const position = registeredPlayers.length - survivingPlayers.length + 1;
    // Compute the tier of the challenge to give to the challenger
    const tier = mapRange(
        position,
        MIN_TIER,
        registeredPlayers.length,
        MIN_TIER,
        MAX_TIER,
    );

    let challengesToGive = null;

    // Get all the challenges that are the computed tier and that have no challenger
    challengesToGive = await Challenges.findAll({
        where: {
            challengeTier: tier,
            challengeIsIrl: challenger.irl,
            challengeChallengerId: null,
        },
    });

    if (challengesToGive.length == 0) {
        let tmpTier = tier;
        while (tmpTier > 1 && challengesToGive.length == 0) {
            tmpTier -= 1;
            challengesToGive = await Challenges.findAll({
                where: {
                    challengeTier: tmpTier,
                    challengeIsIrl: challenger.irl,
                    challengeChallengerId: null,
                },
            });
        }
        if (challengesToGive.length == 0 && challenger.irl) {
            challengesToGive = await Challenges.findAll({
                where: {
                    challengeTier: tier,
                    challengeChallengerId: null,
                },
            });
        }
    }

    // Check if we found challenges from that tier
    if (challengesToGive.length > 0) {
        // Pick a random challenge from this list
        const challengeToGive =
            challengesToGive[
                Math.floor(Math.random() * challengesToGive.length)
            ];

        // Get the id of the challenge to give
        const challengeToGiveId = challengeToGive.get('challengeId');

        // Check if we need to associate players to this challenge and associate
        const associatedChallengers = [];
        const nbChallengersToAssociate = challengeToGive.get(
            'challengeNbAssociatedChallengers',
        );
        if (nbChallengersToAssociate != null && nbChallengersToAssociate > 0) {
            for (let i = 0; i < nbChallengersToAssociate; i++) {
                if (unassociatedPlayers.length == 0) {
                    unassociatedPlayers = registeredPlayers;
                    shuffleArray(unassociatedPlayers);
                }
                let randomUnassociatedPlayerIndex = Math.floor(
                    Math.random() * unassociatedPlayers.length,
                );
                while (
                    unassociatedPlayers[randomUnassociatedPlayerIndex].player
                        .id == challenger.player.id
                ) {
                    randomUnassociatedPlayerIndex = Math.floor(
                        Math.random() * unassociatedPlayers.length,
                    );
                }
                associatedChallengers.push(
                    unassociatedPlayers[randomUnassociatedPlayerIndex],
                );

                unassociatedPlayers.splice(randomUnassociatedPlayerIndex, 1);
            }
        }

        // Assign the challenger and associated challengers to that challenge
        const affectedRows = await Challenges.update(
            {
                challengeChallengerId: challenger.player.id,
                challengeAssociatedChallengersId: associatedChallengers,
            },
            { where: { challengeId: challengeToGiveId } },
        );

        if (affectedRows.length > 0) {
            if (nbChallengersToAssociate > 0) {
                let associatedChallengeMsg =
                    `Pour revenir sur terre, et ainsi pouvoir participer à la prochaine édition de la Shen'Zoulette, ${challenger.player.toString()} doit s'acquitter du challenge :` +
                    `\n\`\`\`${challengeToGive.get('challengeName')}\`\`\`\n` +
                    `Pour s'acquitter de ce challenge, ${challenger.player.toString()} aura besoin de :\n`;
                for (let i = 0; i < associatedChallengers.length; i++) {
                    const associatedChallenger = associatedChallengers[i];
                    associatedChallengeMsg +=
                        `> - ` + associatedChallenger.player.toString() + `\n`;
                }
                msg.channel.send(associatedChallengeMsg);
            } else {
                msg.channel.send(
                    `Pour revenir sur terre, ${challenger.player.username} doit s'acquitter d'un challenge :` +
                        `\n\`\`\`${challengeToGive.get('challengeName')}\`\`\``,
                );
            }
            updateDisplayChallenge();
        } else {
            msg.channel.send("Erreur lors de l'affectation du challenge.");
        }
    } else {
        msg.channel.send(`Pas de challenge trouvé du bon tier (${tier})`);
    }
}

/**
 * Ends the registrations.
 * @param {Message} msg Message object of the command.
 */
function endRegistrations(msg) {
    areRegistrationsRuning = false;

    if (registeredPlayers.length < MIN_PLAYERS) {
        msg.channel.send(
            `Fin de l'enregistrement des joueurs. Il n'y a pas assez d'inscrits. Il faut au moins ${MIN_PLAYERS} joueurs.`,
        );
    } else {
        // Allows the user to kill players.
        canKill = true;

        // Copy the array to be able to empty one of them when players die.
        survivingPlayers = registeredPlayers.slice();

        // Copy the array to be able to empty one of them when players get associated to others challenges
        unassociatedPlayers = registeredPlayers.slice();

        // Randomly sort the array.
        shuffleArray(survivingPlayers);

        // Randomly sort associated players array if needed.
        shuffleArray(unassociatedPlayers);

        msg.channel.send(`Fin de l'enregistrement des joueurs !\n`);

        let listOfPlayers = ``;

        if (registeredPlayers || recentlyRegisteredPlayers) {
            if (!areRegistrationsReopened) {
                listOfPlayers += `**• Liste des participants retenus :**\n`;
                areRegistrationsReopened = false;
                registeredPlayers.forEach(function (challenger) {
                    listOfPlayers +=
                        `> - ` + challenger.player.toString() + `\n`;
                });
            } else {
                if (recentlyRegisteredPlayers.length > 0) {
                    listOfPlayers += `**• Liste des NOUVEAUX participants retenus :**\n`;
                    recentlyRegisteredPlayers.forEach(function (player) {
                        listOfPlayers += `> - ` + player.toString() + `\n`;
                    });
                    recentlyRegisteredPlayers = [];
                }
            }
            if (listOfPlayers) {
                msg.channel.send(listOfPlayers);
            }
        }

        msg.channel.send(
            `\nUtilisez la commande \`${process.env.PREFIX}kill\` pour commencer l'exécution\n` +
                `ou la commande \`${process.env.PREFIX}reopen\` pour ré-ouvrir les enregistrements !`,
        );
    }
}

/**
 * Clear challenges challengers
 */
async function clearChallengersFromChallenges() {
    await Challenges.update(
        { challengeChallengerId: null },
        { where: { challengeChallengerId: { [Op.ne]: null } } },
    );
}

/**
 * Will shuffle an array using the Durstenfeld shuffle.
 * @param {Array} array Array to shuffle.
 */
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

/**
 * Stop the execution of the code for the given amount of time.
 * @param {number} ms Time to wait in ms.
 * @return {Promise}
 */
function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

/**
 * linearly maps value from the range (a..b) to (c..d)
 * @param {number} value
 * @param {number} a
 * @param {number} b
 * @param {number} c
 * @param {number} d
 * @return {number}
 */
function mapRange(value, a, b, c, d) {
    // first map value from (a..b) to (0..1)
    value = (value - a) / (b - a);
    // then map it from (0..1) to (c..d) and return it
    return Math.round(c + value * (d - c));
}

client.login(process.env.DISCORD_TOKEN);
