# Bot Roulette

# Getting started

## Install Node.js

First, install Node.js.

On linux, follow this guide: https://nodejs.org/en/download/package-manager/.

On windows you can install it from there: https://nodejs.org/en/.

## Install the packages

After cloning the repo, go the the root of it and do `npm install`. It will install the the necessary packages for the project.

## The dotenv file

This application uses a `.env` file that stores every global parameters (ex: the prefix) and sensitives information such as the bot token.

A `.env` must be created after cloning the project.

Content of the file:
```
PREFIX=*
DISCORD_TOKEN=
```

The token must be taken from the application page on discord's website.

## Create the database file

The project needs an sqlite file to work. To create it, run the command: `sqlite3 challenges.db`. 

> You can close sqlite with `.exit`

## Setting up a linter

This is optional, but you can setup a linter for this project with this guide: https://discordjs.guide/preparations/setting-up-a-linter.html#setting-up-eslint-rules
